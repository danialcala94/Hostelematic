package es.hostelematic.core;

public class Initializer {

	public static void main(String[] args) {
		System.out.println("Application initialized.");
		
		Core core = Core.getInstance(1);
		core.execute();
		
		System.out.println("Application ended.");
	}

}
