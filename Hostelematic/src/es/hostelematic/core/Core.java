package es.hostelematic.core;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import es.hostelematic.hibernate.models.Alergenos;
import es.hostelematic.hibernate.models.CategoriasEmpleados;
import es.hostelematic.hibernate.models.CategoriasIngredientes;
import es.hostelematic.hibernate.models.CategoriasPlatos;
import es.hostelematic.hibernate.utils.HibernateUtils;

public class Core {
	
	private int id = -3;
	private static Core instance = null;
	
	ArrayList<Alergenos> alergens = new ArrayList<Alergenos>();
	ArrayList<CategoriasEmpleados> workerCategories = new ArrayList<CategoriasEmpleados>();
	ArrayList<CategoriasIngredientes> ingredientsCategories = new ArrayList<CategoriasIngredientes>();
	ArrayList<CategoriasPlatos> dishesCategories = new ArrayList<CategoriasPlatos>();
	
	
	private Core(int id) {
		this.id = id;
	}

	public static Core getInstance(int id) {
		Core inst = Core.instance;
		if (inst == null)
			inst = new Core(id);
		return inst;
	}
	
	
	public void refreshData() {
		Session session;
		
		SessionFactory sf = null;
		sf = HibernateUtils.getSessionFactory();
		session = sf.openSession();
		Transaction tx = session.beginTransaction();
		
		//this.alergens = new ArrayList<Alergenos>(session.createQuery("FROM alergenos").list());
		List<Alergenos> alergens_list = session.createQuery("FROM alergenos").list();
		for (Alergenos alergen : alergens_list)
			System.out.println(alergen.getNombre());
		
		tx.commit();
		session.close();
	}
	
	
	public void execute() {
		System.out.println("Executing...");
		hibernateTests();
		refreshData();
	}
	
	private void hibernateTests() {
		Session session;
		
		SessionFactory sf = null;
		sf = HibernateUtils.getSessionFactory();
		session = sf.openSession();
		
		Transaction tx = session.beginTransaction();
		Alergenos alergeno = session.get(Alergenos.class, 1);
		System.out.println("Alergen " + alergeno.getNombre() + " found with description " + alergeno.getDescripcion());
		CategoriasEmpleados catEmp = session.get(CategoriasEmpleados.class, 1);
		System.out.println("Worker category " + catEmp.getNombre() + " found with description " + catEmp.getDescripcion());
		CategoriasIngredientes catIng = session.get(CategoriasIngredientes.class, 1);
		System.out.println("Ingredient category " + catIng.getNombre() + " found with description " + catIng.getDescripcion());
		CategoriasPlatos catPla= session.get(CategoriasPlatos.class, 1);
		System.out.println("Dish category " + catPla.getNombre() + " found with description " + catPla.getDescripcion());
		
		
		tx.commit();

		session.close();
	}
	
}
